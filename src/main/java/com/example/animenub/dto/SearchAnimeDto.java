package com.example.animenub.dto;

public class SearchAnimeDto {
    private String genres;

    private String setting;

    private String theme;

    public SearchAnimeDto(String genres, String setting, String theme) {
        this.genres = genres;
        this.setting = setting;
        this.theme = theme;
    }

    public String getGenres() {
        return genres;
    }

    public String getSetting() {
        return setting;
    }

    public String getTheme() {
        return theme;
    }
}
