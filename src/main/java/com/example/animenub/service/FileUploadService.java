package com.example.animenub.service;

import com.example.animenub.exception.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class FileUploadService {

    private final Path uploadsFolder = Paths.get("src/main/resources/static");

    public String savePhoto( MultipartFile file) {
        if (file.isEmpty()) {
            throw new CustomException("File is empty", HttpStatus.BAD_REQUEST);
        }

        if (!Files.exists(uploadsFolder)) {
            try {
                Files.createDirectory(uploadsFolder);
            } catch (Exception e) {
                throw new CustomException("Error while create root directory", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        String filename = UUID.randomUUID() + file.getOriginalFilename();
        try {
            byte[] bytes = file.getBytes();
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(
                            new File(uploadsFolder.toAbsolutePath().toString(), filename)
                    )
            );
            stream.write(bytes);
            stream.close();
        } catch (Exception e) {
            throw new CustomException("Error while saving file", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return filename;
    }
}
