package com.example.animenub.controller;

import com.example.animenub.dto.CreateCommentDto;
import com.example.animenub.entity.Comment;
import com.example.animenub.exception.CustomException;
import com.example.animenub.service.CommentsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/comments")
public class CommentsController {

    private final CommentsService commentsService;

    public CommentsController(CommentsService commentsService) {
        this.commentsService = commentsService;
    }

    @PostMapping
    @ApiOperation(
            value = "Create comment",
            notes = "Create comment",
            response = HttpStatus.class
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "When success, returns created http status", response = HttpStatus.class),
            @ApiResponse(code = 400, message = "You already created comment or provide invalid rating", response = CustomException.class),
    })
    public HttpStatus create(@AuthenticationPrincipal User user, @RequestBody CreateCommentDto commentDto) {
        commentsService.create(commentDto, user.getUsername());

        return HttpStatus.CREATED;
    }

    @GetMapping("/{animeId}")
    @ApiOperation(
            value = "Get all comments by anime id",
            notes = "Get all comments by anime id",
            response = List.class
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns all comments. If comments not found returns empty list", response = List.class),
    })
    public List<Comment> getCommentsByAnimeId(@PathVariable("animeId") String animeId) {
        return commentsService.getCommentsByAnimeId(animeId);
    }
}
