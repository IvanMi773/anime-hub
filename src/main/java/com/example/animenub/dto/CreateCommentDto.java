package com.example.animenub.dto;

import java.util.Date;

public class CreateCommentDto {

    private String text;
    private Date createdAt;
    private Integer rating;
    private String animeId;

    public CreateCommentDto(String text, Date createdAt, Integer rating, String animeId) {
        this.text = text;
        this.createdAt = createdAt;
        this.rating = rating;
        this.animeId = animeId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getAnimeId() {
        return animeId;
    }

    public void setAnimeId(String animeId) {
        this.animeId = animeId;
    }
}
