package com.example.animenub.controller;

import com.example.animenub.dto.SaveAnimeDto;
import com.example.animenub.dto.SearchAnimeDto;
import com.example.animenub.entity.Anime;
import com.example.animenub.service.AnimeRecomendationsService;
import com.example.animenub.service.AnimesService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class AnimeController {
    private final AnimesService animesService;
    private final AnimeRecomendationsService animeRecomendationsService;

    public AnimeController(AnimesService animesService, AnimeRecomendationsService animeRecomendationsService) {
        this.animesService = animesService;
        this.animeRecomendationsService = animeRecomendationsService;
    }

    @PostMapping("/user/anime")
    @ApiOperation(
            value = "Add new anime",
            notes = "Add new anime",
            response = ResponseEntity.class
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "If success, returns new anime", response = ResponseEntity.class),
            @ApiResponse(code = 400, message = "Bad credentials or empty file", response = Exception.class),
            @ApiResponse(code = 500, message = "Error while uploading photo", response = Exception.class)
    })
    public ResponseEntity<Anime> save(
            @AuthenticationPrincipal User user,
            @RequestParam("country") String country,
            @RequestParam("genres") String genres,
            @RequestParam("setting") String setting,
            @RequestParam("theme") String theme,
            @RequestParam("title") String title,
            @RequestParam("file") MultipartFile file
    ) {
        var dto = new SaveAnimeDto(title, genres, setting, theme, country, file);
        return ResponseEntity.ok().body(animesService.save(dto, user.getUsername()));
    }

    @GetMapping("/anime/{id}")
    @ApiOperation(
            value = "Get anime by id",
            notes = "Get anime by id",
            response = ResponseEntity.class
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "If success, returns anime", response = ResponseEntity.class),
            @ApiResponse(code = 400, message = "Bad credentials", response = Exception.class),
    })
    public ResponseEntity<Anime> getOne(@PathVariable("id") String id) {
        return ResponseEntity.ok().body(animesService.getOne(id));
    }

    @GetMapping("/anime/recomendations")
    @ApiResponses({
            @ApiResponse(code = 200, message = "If success, returns anime", response = ResponseEntity.class),
    })
    public ResponseEntity<List<Anime>> getRecomendations(
            @RequestParam("genres") String genres,
            @RequestParam("setting") String setting,
            @RequestParam("theme") String theme
    ) {
        SearchAnimeDto dto = new SearchAnimeDto(genres, setting, theme);
        return ResponseEntity
                .ok()
                .body(animeRecomendationsService.getRecomendations(dto));
    }

    @GetMapping("/anime")
    @ApiOperation(
            value = "Get all animes",
            notes = "Get all animes",
            response = ResponseEntity.class
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "If success, returns list of anime", response = ResponseEntity.class),
            @ApiResponse(code = 400, message = "Bad credentials", response = Exception.class),
    })
    public ResponseEntity<List<Anime>> getAll() {
        return ResponseEntity.ok().body(animesService.getAll());
    }
}
