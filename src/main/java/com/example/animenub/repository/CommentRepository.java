package com.example.animenub.repository;

import com.example.animenub.entity.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends MongoRepository<Comment, String> {

    Optional<Comment> findCommentsByAnimeIdAndUsername(String animeId, String username);
    List<Comment> findAllByAnimeId(String animeId);
}
