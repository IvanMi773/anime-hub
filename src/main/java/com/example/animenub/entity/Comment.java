package com.example.animenub.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Date;

@Document
public class Comment {

    @Id
    private String id;

    private String username;

    private String text;

    private Date createdAt;

    @Field(targetType = FieldType.OBJECT_ID)
    private String animeId;

    @Min(1)
    @Max(5)
    private Integer rating;

    public Comment(String username, String text, Date createdAt, String animeId, Integer rating) {
        this.username = username;
        this.text = text;
        this.createdAt = createdAt;
        this.animeId = animeId;
        this.rating = rating;
    }

    public Comment() {
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getAnimeId() {
        return animeId;
    }

    public void setAnimeId(String animeId) {
        this.animeId = animeId;
    }
}
