package com.example.animenub.repository;

import com.example.animenub.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    void whenFindByUsernameShouldReturnUser() {
        var user = new User("test-username","password");

        userRepository.save(user);
        Optional<User> dbUser = userRepository.findByUsername("test-username");

        assertThat(dbUser.isEmpty()).isFalse();

        userRepository.deleteAll();
    }

    @Test
    void whenFindByUsernameShouldntReturnUser() {
        var user = new User("test-username","password");

        userRepository.save(user);
        Optional<User> dbUser = userRepository.findByUsername("test");

        assertThat(dbUser.isEmpty()).isTrue();

        userRepository.deleteAll();
    }
}