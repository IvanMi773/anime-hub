package com.example.animenub.service;

import com.example.animenub.dto.SearchAnimeDto;
import com.example.animenub.entity.Anime;
import com.example.animenub.repository.AnimeRepository;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class AnimeRecomendationsService {
    private final AnimeRepository animeRepository;

    public AnimeRecomendationsService(AnimeRepository animeRepository) {
        this.animeRepository = animeRepository;
    }


    public List<Anime> getRecomendations(SearchAnimeDto dto) {
        System.out.println(dto.getTheme());
        var queryResult = animeRepository.getListByCriteriaMatches(
                dto.getGenres().split(" "),
                dto.getSetting().split(" "),
                dto.getTheme().split(" ")
        );

        return queryResult;
    }
}
