package com.example.animenub.service;

import com.example.animenub.dto.CreateCommentDto;
import com.example.animenub.entity.Comment;
import com.example.animenub.exception.CustomException;
import com.example.animenub.repository.CommentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentsService {

    private final CommentRepository commentRepository;

    public CommentsService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public void create(CreateCommentDto commentDto, String username) {
        if (commentRepository.findCommentsByAnimeIdAndUsername(commentDto.getAnimeId(), username).isEmpty()) {
            var comment = new Comment(
                    username,
                    commentDto.getText(),
                    commentDto.getCreatedAt(),
                    commentDto.getAnimeId(),
                    commentDto.getRating()
            );
            commentRepository.save(comment);

            return;
        }

        throw new CustomException("You already created a comment", HttpStatus.BAD_REQUEST);
    }

    public List<Comment> getCommentsByAnimeId(String animeId) {
        return commentRepository.findAllByAnimeId(animeId);
    }
}
