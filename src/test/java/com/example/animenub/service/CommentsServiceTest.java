package com.example.animenub.service;

import com.example.animenub.dto.CreateCommentDto;
import com.example.animenub.entity.Anime;
import com.example.animenub.entity.Comment;
import com.example.animenub.repository.AnimeRepository;
import com.example.animenub.repository.CommentRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CommentsServiceTest {

    @Mock
    private CommentRepository commentRepository;

//    @Mock
//    private AnimeRepository animeRepository;

    @InjectMocks
    private CommentsService commentsService;

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void setDown() {
        commentRepository.deleteAll();
//        animeRepository.deleteAll();
    }

    @Test
    void whenCreateShouldSaveCommentToDb() {
//        var anime = new Anime("title", "genres", "setting", "theme", "country", "addedBy", "image");
//        animeRepository.save(anime);


//        var commentDto = new CreateCommentDto("text", new Date(), 3, "anime.getId()");
//
//
//        when(commentRepository.findCommentsByUsername(anyString())).thenReturn(Optional.of(Mockito.mock(Comment.class)));
//        commentsService.create(commentDto, "test-username");
//        verify(commentRepository).save(any());
    }

    @Test
    void getCommentsByAnimeId() {
    }
}