package com.example.animenub.dto;

import org.springframework.web.multipart.MultipartFile;

public class SaveAnimeDto {
    private String title;

    private String genres;

    private String setting;

    private String theme;

    private String country;

    private MultipartFile file;

    public SaveAnimeDto(String title, String genres, String setting, String theme, String country, MultipartFile file) {
        this.title = title;
        this.genres = genres;
        this.setting = setting;
        this.theme = theme;
        this.country = country;
        this.file = file;
    }

    public String getTitle() {
        return title;
    }

    public String getGenres() {
        return genres;
    }

    public String getSetting() {
        return setting;
    }

    public String getTheme() {
        return theme;
    }

    public String getCountry() {
        return country;
    }

    public MultipartFile getFile() {
        return file;
    }
}
