package com.example.animenub.entity;

import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Anime {
    @Id
    private String id;

    private String title;

    private String genres;

    private String setting;

    private String theme;

    private String country;

    private String addedBy;

    private String image;

    public Anime(String title, String genres, String setting, String theme, String country, String addedBy, String image) {
        this.title = title;
        this.genres = genres;
        this.setting = setting;
        this.theme = theme;
        this.country = country;
        this.image = image;
        this.addedBy = addedBy;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenres() {
        return genres;
    }

    public String getSetting() {
        return setting;
    }

    public String getTheme() {
        return theme;
    }

    public String getCountry() {
        return country;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public String getImage() {
        return image;
    }
}
