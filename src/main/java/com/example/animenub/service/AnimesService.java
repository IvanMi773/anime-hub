package com.example.animenub.service;

import com.example.animenub.dto.SaveAnimeDto;
import com.example.animenub.entity.Anime;
import com.example.animenub.exception.CustomException;
import com.example.animenub.repository.AnimeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnimesService {

    private final AnimeRepository animeRepository;
    private final FileUploadService fileUploadService;

    public AnimesService(AnimeRepository animeRepository, FileUploadService fileUploadService) {
        this.animeRepository = animeRepository;
        this.fileUploadService = fileUploadService;
    }

    public Anime save(SaveAnimeDto dto, String userLogin) {
        var filename = fileUploadService.savePhoto(dto.getFile());

        Anime anime = new Anime(
                dto.getTitle(),
                dto.getGenres(),
                dto.getSetting(),
                dto.getTheme(),
                dto.getCountry(),
                userLogin,
                filename
        );

        return animeRepository.save(anime);
    }

    public Anime getOne(String id) {
        return animeRepository.findById(id).orElseThrow(
                () -> new CustomException("Anime not found", HttpStatus.NOT_FOUND)
        );
    }

    public List<Anime> getAll() {
        return animeRepository.findAll();
    }
}
