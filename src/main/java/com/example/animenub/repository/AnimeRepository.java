package com.example.animenub.repository;

import com.example.animenub.entity.Anime;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimeRepository extends MongoRepository<Anime, String> {

    // TODO: add * or | instead of spaces (replace
    @Query(
            "{ $or: [\n" +
            "{ 'genres': { $in: ?0 } },\n" +
            "{ 'setting': { $in: ?1 } },\n" +
            "{ 'theme': { $in: ?2 } }\n" +
            "] }")
    List<Anime> getListByCriteriaMatches(String[] genres, String[] setting, String[] theme);
}
