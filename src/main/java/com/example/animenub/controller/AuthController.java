package com.example.animenub.controller;

import com.example.animenub.dto.UserAuthorizationDto;
import com.example.animenub.service.security.AuthorizationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final AuthorizationService authorizationService;

    public AuthController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @PostMapping
    @ApiOperation(
            value = "Authorize user",
            notes = "Authorize user",
            response = ResponseEntity.class
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "When success, returns jwt token", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Bad credentials", response = Exception.class),
    })
    public ResponseEntity<String> authorize (@RequestBody UserAuthorizationDto userDto) {
        return ResponseEntity.ok().body(authorizationService.authorize(userDto));
    }
}
