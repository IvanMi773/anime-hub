package com.example.animenub.repository;

import com.example.animenub.entity.Anime;
import com.example.animenub.entity.Comment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class CommentRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private AnimeRepository animeRepository;

    private Anime anime;

    @BeforeEach
    void setUp() {
        anime = new Anime("title", "genres", "setting", "theme", "country", "addedBy", "image");
        animeRepository.save(anime);

        var comment = new Comment("test-username", "some text", new Date(), anime.getId(), 4);
        commentRepository.save(comment);
    }

    @AfterEach
    void setDown() {
        commentRepository.deleteAll();
        animeRepository.deleteAll();
    }

//    @Test
//    void whenFindCommentsByUsernameShouldReturnComment() {
//        var dbComment = commentRepository.findCommentsByUsername("test-username");
//        assertThat(dbComment.isEmpty()).isFalse();
//    }
//
//    @Test
//    void whenFindCommentsByUsernameShouldntReturnComment() {
//        var dbComment = commentRepository.findCommentsByUsername("username");
//        assertThat(dbComment.isEmpty()).isTrue();
//    }

    @Test
    void whenFindAllByAnimeIdShouldReturnEmptyList() {
        var anime2 = new Anime("title", "genres", "setting", "theme", "country", "addedBy", "image");
        animeRepository.save(anime2);

        var comment2 = new Comment("test-username", "some text", new Date(), anime.getId(), 4);
        commentRepository.save(comment2);

        var dbComment = commentRepository.findAllByAnimeId(anime2.getId());
        assertThat(dbComment.size()).isEqualTo(0);
    }

    @Test
    void whenFindAllByAnimeIdShouldReturnListWithSize2() {
        var comment2 = new Comment("test-username", "some text", new Date(), anime.getId(), 4);
        commentRepository.save(comment2);

        var dbComment = commentRepository.findAllByAnimeId(anime.getId());
        assertThat(dbComment.size()).isEqualTo(2);
    }
}