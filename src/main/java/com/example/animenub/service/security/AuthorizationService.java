package com.example.animenub.service.security;

import com.example.animenub.dto.UserAuthorizationDto;
import com.example.animenub.entity.User;
import com.example.animenub.exception.CustomException;
import com.example.animenub.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProviderService jwtTokenProviderService;
    private final PasswordEncoder passwordEncoder;

    public AuthorizationService(UserRepository userRepository, AuthenticationManager authenticationManager, JwtTokenProviderService jwtTokenProviderService, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProviderService = jwtTokenProviderService;
        this.passwordEncoder = passwordEncoder;
    }

    public String authorize(UserAuthorizationDto userDto) {
        var user = userRepository.findByUsername(userDto.getUsername());

        if (user.isPresent()) {
            return login(userDto);
        } else {
            return registration(userDto);
        }
    }

    private String registration(UserAuthorizationDto userDto) {
        var user = new User(userDto.getUsername(), userDto.getPassword());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);

        return jwtTokenProviderService.generateToken(user.getUsername(), "user");
    }

    private String login(UserAuthorizationDto userDto) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userDto.getUsername(), userDto.getPassword()));

            return jwtTokenProviderService.generateToken(userDto.getUsername(), "user");
        } catch (AuthenticationException e) {
            throw new CustomException("Error with logging in", HttpStatus.UNAUTHORIZED);
        }
    }
}
